# Front End Case Study

This is the case study application for myRetail built from the [React Create App](https://github.com/facebook/create-react-app) starter.

## Overview

There are a few quick things to note on the functional side:

* Redux in used here to highlight usage in an application (albeit quite minimally here). There are two actions and reducers used to:
  * load json data (on load)
  * add the item and quantity to a cart (when clicking "add to cart")
* The carousel is functional and thumbnails are selectable to change the main image
* The (+) and (-) buttons work to update the quantity input
* The app is fully responsive

## Setup

Install dependencies:

```sh
yarn
```

## Running locally

Run the local development environment:

```sh
yarn start
```

## Tests

Run the included tests:

```sh
yarn test
```

## Deployment Using Continuous Delivery

Since this is an entirely front-end, static application, simply building the app and deploying the built static assets to a http server will suffice.

An approach to using continuous delivery for this project would be as follows:

A Jenkins CI server would be setup to accept git hooks on push for this repository on a remote git server (Github, Gitlab, etc.). When a user pushes a new tag to this git server (using semver rules), Jenkins would check to see if a new tag has been made for master. If this is true, the deployment pipeline will be initiated.

Jenkins will this pull down the contents of the repository, install packages and run all unit tests. If any tests fail, the deployment pipeline is terminated. Otherwise, the project will be built and copied to a remote staging server. At this point the staging environment can be tested manually or with an automated suite of tests.

After staging has been verified, a user must initiate the "deploy to prod" action via Jenkins. The project is now rebuilt using any production settings, and deployed to production.

A flow chart of this process can be found in "docs" in this repository.

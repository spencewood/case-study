export const loadProductDetails = payload => ({
  type: 'LOAD_PRODUCT_DETAILS',
  payload
});

export const productAddedToCart = payload => ({
  type: 'PRODUCT_ADDED_TO_CART',
  payload
});

export const productAddToCart = payload => dispatch => {
  alert(`PRODUCT_ADD_TO_CART: \n\n${JSON.stringify(payload)}`);
  // send a xhr request to update cart...
  setTimeout(() => dispatch(productAddedToCart({})), 500);
};

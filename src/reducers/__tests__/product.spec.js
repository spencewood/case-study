import reducer from '../product';

describe('Product reducers', () => {
  it('should load initial state', () => {
    expect(reducer()).toEqual({});
  });

  describe('LOAD_PRODUCT_DETAILS', () => {
    it('should not error if structure is undefined', () => {
      const action = {
        type: 'LOAD_PRODUCT_DETAILS'
      };
      expect(reducer({}, action)).toEqual({});
    });

    it('should not error if CatalogEntryView contains incorrect initial structure', () => {
      const action = {
        type: 'LOAD_PRODUCT_DETAILS',
        payload: {
          CatalogEntryView: {}
        }
      };
      expect(reducer({}, action)).toEqual({});
    });

    it('should put conforming payload into state', () => {
      const action = {
        type: 'LOAD_PRODUCT_DETAILS',
        payload: {
          CatalogEntryView: [
            {
              some: 'value'
            }
          ]
        }
      };
      expect(reducer({}, action)).toEqual({
        some: 'value'
      });
    });
  });
});

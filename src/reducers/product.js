import get from 'lodash.get';
import pick from 'lodash.pick';
const defaultProduct = {};

const adaptDetails = details => {
  if (details && details.CatalogEntryView) {
    if (Array.isArray(details.CatalogEntryView)) {
      const [detail] = details.CatalogEntryView;
      return {
        ...detail
      };
    }
  }
  return defaultProduct;
};

const product = (state = defaultProduct, action = {}) => {
  const { type, payload } = action;
  switch (type) {
    case 'LOAD_PRODUCT_DETAILS':
      return adaptDetails(payload);
    default:
      return state;
  }
};

export default product;

// SELECTORS

// images
const getImages = state => get(state, 'product.Images[0]');

export const getPrimaryImageUrl = state =>
  get(getImages(state), 'PrimaryImage[0].image');

export const getAlternateImageUrls = state =>
  get(getImages(state), 'AlternateImages', []).map(img => img.image);

// price
export const getPrice = state => get(state, 'product.Offers[0].OfferPrice[0]');

// promos
export const getPromos = state =>
  get(state, 'product.Promotions', []).map(promo =>
    get(promo, 'Description[0].shortDescription')
  );

// features
export const getFeatures = state =>
  get(state, 'product.ItemDescription[0].features');

// reviews
const getReview = state => get(state, 'product.CustomerReview[0]', {});

export const getProReview = state => get(getReview(state), 'Pro[0]');

export const getConReview = state => get(getReview(state), 'Con[0]');

export const getReviewDetails = state =>
  pick(getReview(state), [
    'consolidatedOverallRating',
    'totalPages',
    'totalReviews'
  ]);

export const getAllReviews = state => getReview(state).Reviews;

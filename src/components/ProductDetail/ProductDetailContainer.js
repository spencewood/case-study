import React, { Component } from 'react';
import { connect } from 'react-redux';
import ProductDetail from './ProductDetail';
import { loadProductDetails } from '../../actions';
import {
  getPrimaryImageUrl,
  getAlternateImageUrls,
  getPrice,
  getPromos,
  getFeatures,
  getProReview,
  getConReview,
  getReviewDetails
} from '../../reducers/product';
import data from '../../data/item-data.json';

class ProductDetailContainer extends Component {
  componentDidMount() {
    // load our local json data. alternatively, this could be a remote call...
    this.props.loadProductDetails(data);
  }

  render() {
    return <ProductDetail {...this.props} />;
  }
}

const mapStateToProps = state => ({
  productName: state.product.title,
  primaryImageUrl: getPrimaryImageUrl(state),
  alternateImageUrls: getAlternateImageUrls(state),
  price: getPrice(state),
  promos: getPromos(state),
  features: getFeatures(state),
  reviews: {
    pro: getProReview(state),
    con: getConReview(state),
    details: getReviewDetails(state)
  }
});

const mapDispatchToProps = dispatch => ({
  loadProductDetails: details => dispatch(loadProductDetails(details))
});

export default connect(mapStateToProps, mapDispatchToProps)(
  ProductDetailContainer
);

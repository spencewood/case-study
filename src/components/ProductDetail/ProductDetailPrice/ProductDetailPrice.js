import React from 'react';
import PropTypes from 'prop-types';
import './ProductDetailPrice.css';

const ProductDetailPrice = ({ price, qualifier, promos }) => (
  <div>
    <div className="product-detail-price">
      <span className="product-detail-price--price">{price}</span>
      <span className="product-detail-price--qualifier">{qualifier}</span>
    </div>
    <div className="product-detail-promos">
      <ul className="product-detail-promos--list">
        {promos && promos.map((promo, idx) => <li key={idx}>{promo}</li>)}
      </ul>
    </div>
  </div>
);

ProductDetailPrice.propTypes = {
  price: PropTypes.string,
  qualifier: PropTypes.string,
  promos: PropTypes.arrayOf(PropTypes.string)
};

ProductDetailPrice.defaultProps = {
  price: '$0.00'
};

export default ProductDetailPrice;

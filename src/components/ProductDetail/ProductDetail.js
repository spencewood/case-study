import React from 'react';
import PropTypes from 'prop-types';
import ProductDetailFocus from './ProductDetailFocus';
import ProductDetailPrice from './ProductDetailPrice';
import ProductDetailPurchase from './ProductDetailPurchase';
import ProductDetailHighlights from './ProductDetailHighlights';
import ProductDetailReviews from './ProductDetailReviews';

import './ProductDetail.css';

const ProductDetail = ({
  productName,
  primaryImageUrl,
  alternateImageUrls,
  price,
  promos,
  features,
  reviews
}) => (
  <div className="product-detail">
    <div className="row">
      <div className="col">
        <ProductDetailFocus
          productName={productName}
          imageUrl={primaryImageUrl}
          alternateImageUrls={alternateImageUrls}
        />
      </div>
      <div className="col">
        <ProductDetailPrice
          price={price.formattedPriceValue}
          qualifier={price.priceQualifier}
          promos={promos}
        />
        <ProductDetailPurchase />
        <ProductDetailHighlights features={features} />
      </div>
    </div>
    <div className="row">
      <div className="col-lg-6">
        <ProductDetailReviews
          pro={reviews.pro}
          con={reviews.con}
          details={reviews.details}
        />
      </div>
    </div>
  </div>
);

ProductDetail.propTypes = {
  productName: PropTypes.string,
  primaryImageUrl: PropTypes.string,
  alternateImageUrls: PropTypes.arrayOf(PropTypes.string),
  price: PropTypes.shape({
    currencyCode: PropTypes.string,
    formattedPriceValue: PropTypes.string,
    priceQualifier: PropTypes.string,
    priceValue: PropTypes.string
  }),
  promos: PropTypes.arrayOf(PropTypes.string),
  features: PropTypes.arrayOf(PropTypes.string),
  reviews: PropTypes.shape({
    pro: PropTypes.shape({}),
    con: PropTypes.shape({}),
    details: PropTypes.shape({})
  })
};

ProductDetail.defaultProps = {
  price: {}
};

export default ProductDetail;

import React from 'react';
import { mount } from 'enzyme';
import Rating from '../Rating';
import Star from '../Star';

describe('Rating', () => {
  it('should have the right number of stars based on max', () => {
    const wrapper = mount(<Rating max={3} />);
    expect(wrapper.find(Star).length).toBe(3);
  });

  it('should have the right number of "on" stars based on rating', () => {
    const wrapper = mount(<Rating rating={3} />);
    expect(wrapper.find('[data-type="on"]').length).toBe(3);
  });
});

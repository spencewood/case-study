import React from 'react';

const Star = ({ width, height, fill }) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 260 245"
    style={{ width, height }}
  >
    <path style={{ fill, width, height }} d="m55,237 74-228 74,228L9,96h240" />
  </svg>
);

Star.defaultProps = {
  width: '10px',
  height: '10px',
  fill: '#000'
};

export default Star;

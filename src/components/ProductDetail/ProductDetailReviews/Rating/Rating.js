import React from 'react';
import PropTypes from 'prop-types';
import Star from './Star';

const Rating = ({ rating, max, size }) => {
  return [...new Array(max)].map((_, idx) => {
    if (idx < rating) {
      return (
        <Star
          key={idx}
          data-type="on"
          width={size}
          height={size}
          fill="#cc0000"
        />
      );
    }
    return (
      <Star key={idx} data-type="off" width={size} height={size} fill="#999" />
    );
  });
};

Rating.propTypes = {
  max: PropTypes.number,
  rating: PropTypes.number
};

Rating.defaultProps = {
  max: 5,
  rating: 0
};

export default Rating;

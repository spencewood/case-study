import React from 'react';
import PropTypes from 'prop-types';
import ReviewCard from './ReviewCard';
import Rating from './Rating';

import './ProductDetailReviews.css';

const ProductDetailReviews = ({ pro, con, details }) => (
  <div className="product-detail-reviews">
    <div className="row product-detail-reviews--overall">
      <div className="col-7">
        <Rating
          rating={parseInt(details.consolidatedOverallRating, 10)}
          size="24px"
        />
        <span className="product-detail-reviews--overall-text">overall</span>
      </div>
      <div className="col text-right">
        <a href="#" className="product-detail-reviews--view-all">
          view all {details.totalReviews} reviews
        </a>
      </div>
    </div>
    <div className="product-detail-reviews--pros-cons">
      <div className="row product-detail-reviews--header">
        <div className="col product-detail-reviews__padded-content">
          <h5 className="product-detail-reviews--title">Pro</h5>
          <span>most helpful 4-5 star review</span>
        </div>
        <div className="col">
          <h5 className="product-detail-reviews--title">Con</h5>
          <span>most helpful 1-2 star review</span>
        </div>
      </div>
      <div className="row product-detail-reviews--body">
        <div className="col product-detail-reviews__padded-content">
          <ReviewCard
            rating={parseInt(pro.overallRating, 10)}
            title={pro.title}
            review={pro.review}
            screenName={pro.screenName}
            date={pro.datePosted}
          />
        </div>
        <div className="col">
          <ReviewCard
            rating={parseInt(con.overallRating, 10)}
            title={con.title}
            review={con.review}
            screenName={con.screenName}
            date={con.datePosted}
          />
        </div>
      </div>
    </div>
  </div>
);

ProductDetailReviews.propTypes = {
  pro: PropTypes.shape({}),
  con: PropTypes.shape({}),
  details: PropTypes.shape({})
};

ProductDetailReviews.defaultProps = {
  pro: {},
  con: {},
  details: {
    consolidatedOverallRating: 0,
    totalPages: 0,
    totalReviews: 0
  }
};

export default ProductDetailReviews;

import React from 'react';
import PropTypes from 'prop-types';
import dateformat from 'dateformat';
import Rating from '../Rating';

import './ReviewCard.css';

const ReviewCard = ({ rating, title, review, screenName, date }) => (
  <div>
    <Rating rating={parseInt(rating, 10)} size="12px" />
    <span className="review-card--title">{title}</span>
    <div className="review-card--body">{review}</div>
    <div className="review-card--author">
      <a href="#">{screenName}</a>
      <span className="review-card--date">
        {dateformat(date, 'mmmm d, yyyy')}
      </span>
    </div>
  </div>
);

ReviewCard.propTypes = {
  rating: PropTypes.number,
  title: PropTypes.string,
  review: PropTypes.string,
  screenName: PropTypes.string,
  date: PropTypes.string
};

ReviewCard.defaultProps = {
  rating: 0
};

export default ReviewCard;

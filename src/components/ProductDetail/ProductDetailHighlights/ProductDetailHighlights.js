import React from 'react';
import PropTypes from 'prop-types';

import './ProductDetailHighlights.css';

const ProductDetailHighlights = ({ features }) => (
  <div className="product-detail-highlights">
    <h2 className="product-detail-highlights--title">product highlights</h2>
    <ul className="product-detail-highlights--list">
      {features &&
        features.map((feature, idx) => (
          <li dangerouslySetInnerHTML={{ __html: feature }} key={idx} />
        ))}
    </ul>
  </div>
);

ProductDetailHighlights.propTypes = {
  features: PropTypes.arrayOf(PropTypes.string)
};

export default ProductDetailHighlights;

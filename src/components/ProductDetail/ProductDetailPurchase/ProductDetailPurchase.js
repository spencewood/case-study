import React from 'react';
import PropTypes from 'prop-types';
import PurchaseArea from './PurchaseArea';
import ProductReturns from './ProductReturns';
import ListAddAndSocial from './ListAddAndSocial';

const ProductDetailPurchase = ({ onCartAdd, productId, channelCode }) => (
  <div>
    <PurchaseArea
      onCartAdd={onCartAdd}
      productId={productId}
      channelCode={channelCode}
    />
    <ProductReturns />
    <ListAddAndSocial />
  </div>
);

ProductDetailPurchase.propTypes = {
  onCartAdd: PropTypes.func,
  productId: PropTypes.string,
  channelCode: PropTypes.string
};

export default ProductDetailPurchase;

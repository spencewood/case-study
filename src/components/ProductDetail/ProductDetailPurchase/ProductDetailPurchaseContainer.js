import React, { Component } from 'react';
import { connect } from 'react-redux';
import ProductDetailPurchase from './ProductDetailPurchase';
import { productAddToCart } from '../../../actions';

class ProductDetailPurchaseContainer extends Component {
  render() {
    return <ProductDetailPurchase {...this.props} />;
  }
}

const mapStateToProps = state => ({
  channelCode: state.product.purchasingChannelCode,
  productId: state.product.itemId
});

const mapDispatchToProps = dispatch => ({
  onCartAdd: ({ productId, quantity }) =>
    dispatch(productAddToCart({ productId, quantity }))
});

export default connect(mapStateToProps, mapDispatchToProps)(
  ProductDetailPurchaseContainer
);

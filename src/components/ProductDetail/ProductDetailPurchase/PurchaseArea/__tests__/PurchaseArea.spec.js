import React from 'react';
import { shallow } from 'enzyme';
import PurchaseArea from '../PurchaseArea';
import PickUpInStoreButton from '../PickUpInStoreButton';
import AddToCartButton from '../AddToCartButton';

describe('Product Detail Purchase', () => {
  // Show the add to cart button only if the item is available online, purchasingChannelCode equals 0 or 1.

  it('should show "add to cart" button if purchasingChannelCode equals "0"', () => {
    const wrapper = shallow(<PurchaseArea channelCode="0" />);
    expect(wrapper.find(AddToCartButton).length).toBe(1);
  });

  it('should show "add to cart" button if purchasingChannelCode equals "1"', () => {
    const wrapper = shallow(<PurchaseArea channelCode="1" />);
    expect(wrapper.find(AddToCartButton).length).toBe(1);
  });

  it('should not show "add to cart" button if purchingsChannelCode is undefined', () => {
    const wrapper = shallow(<PurchaseArea />);
    expect(wrapper.find(AddToCartButton).length).toBe(0);
  });

  it('should not show the "add to cart" button for other numbers', () => {
    const wrapper = shallow(<PurchaseArea channelCode="3" />);
    expect(wrapper.find(AddToCartButton).length).toBe(0);
  });

  // Show the pick up in store only if the item is available instore, purchasingChannelCode equals 0 or 2.

  it('should show "add to cart" button if purchasingChannelCode equals "0"', () => {
    const wrapper = shallow(<PurchaseArea channelCode="0" />);
    expect(wrapper.find(PickUpInStoreButton).length).toBe(1);
  });

  it('should show "add to cart" button if purchasingChannelCode equals "1"', () => {
    const wrapper = shallow(<PurchaseArea channelCode="2" />);
    expect(wrapper.find(PickUpInStoreButton).length).toBe(1);
  });

  it('should not show "add to cart" button if purchingsChannelCode is undefined', () => {
    const wrapper = shallow(<PurchaseArea />);
    expect(wrapper.find(PickUpInStoreButton).length).toBe(0);
  });

  it('should not show the "add to cart" button for other numbers', () => {
    const wrapper = shallow(<PurchaseArea channelCode="3" />);
    expect(wrapper.find(PickUpInStoreButton).length).toBe(0);
  });
});

import React from 'react';

const PickUpInStoreButton = () => (
  <div className="mx-auto">
    <button
      className="purchase-area--button purchase-area--store-pickup"
      data-button-name="store-pickup"
    >
      Pick Up In Store
    </button>
    <span className="d-none d-lg-block d-xl-block">find in a store</span>
  </div>
);

export default PickUpInStoreButton;

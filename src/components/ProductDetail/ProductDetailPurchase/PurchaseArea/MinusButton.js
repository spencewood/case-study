import React from 'react';

const MinusButton = ({ onClick }) => (
  <div
    className="purchase-area-input--button purchase-area-input--button__minus"
    onClick={onClick}
  >
    <svg
      id="QTY_copy"
      data-name="QTY copy"
      xmlns="http://www.w3.org/2000/svg"
      width="30"
      height="30"
      viewBox="0 0 30 30"
    >
      <defs />
      <circle
        id="Ellipse_2_copy"
        data-name="Ellipse 2 copy"
        cx="15"
        cy="15"
        r="15"
        style={{ fill: '#ccc' }}
      />
      <text
        id="_"
        data-name="–"
        transform="translate(7 22.949) scale(0.507 0.505)"
        style={{
          fontSize: '59.463px',
          fill: '#fff',
          fontFamily: 'HelveticaNeueLTStd-Lt, HelveticaNeueLTStd'
        }}
      >
        –
      </text>
    </svg>
  </div>
);

export default MinusButton;

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PickUpInStoreButton from './PickUpInStoreButton';
import AddToCartButton from './AddToCartButton';
import MinusButton from './MinusButton';
import PlusButton from './PlusButton';

import './PurchaseArea.css';

class PurchaseArea extends Component {
  constructor(props) {
    super(props);

    this.state = {
      quantity: 0
    };
  }

  updateQuantity = ({ target }) => {
    this.setState({
      quantity: target.value
    });
  };

  addToCart = () => {
    this.props.onCartAdd({
      quantity: this.state.quantity,
      productId: this.props.productId
    });
  };

  increaseQuantity = () => {
    this.setState({
      quantity: this.state.quantity + 1
    });
  };

  decreaseQuantity = () => {
    this.setState({
      quantity: this.state.quantity - 1
    });
  };

  render() {
    const { channelCode } = this.props;
    return (
      <div>
        <div className="row product-detail-purchase--quantity-container">
          <div className="col-6 col-md-12 col-lg-6">
            <label className="purchase-area--quantity-label">quantity:</label>
            <MinusButton onClick={this.decreaseQuantity} />
            <PlusButton onClick={this.increaseQuantity} />
            <input
              className="purchase-area--quantity-input"
              disabled
              name="quantity"
              value={this.state.quantity}
              onChange={this.updateQuantity}
            />
          </div>
        </div>
        <div className="row product-detail-purchase--button-container">
          <div className="col">
            {channelCode === '0' || channelCode === '2' ? (
              <PickUpInStoreButton />
            ) : null}
          </div>
          <div className="col">
            {channelCode === '0' || channelCode === '1' ? (
              <AddToCartButton onAddToCart={this.addToCart} />
            ) : null}
          </div>
        </div>
      </div>
    );
  }
}

PurchaseArea.propTypes = {
  onCartAdd: PropTypes.func,
  productId: PropTypes.string,
  channelCode: PropTypes.string
};

export default PurchaseArea;

import React from 'react';
import PropTypes from 'prop-types';

const AddToCartButton = ({ onAddToCart }) => (
  <button
    className="purchase-area--button purchase-area--add-to-cart"
    data-button-name="add-to-cart"
    onClick={onAddToCart}
  >
    Add to Cart
  </button>
);

AddToCartButton.propTypes = {
  onAddToCart: PropTypes.func
};

export default AddToCartButton;

import React from 'react';

const PlusButton = ({ onClick }) => (
  <div
    className="purchase-area-input--button purchase-area-input--button__plus"
    onClick={onClick}
  >
    <svg
      id="QTY_copy"
      data-name="QTY copy"
      xmlns="http://www.w3.org/2000/svg"
      width="30"
      height="30"
      viewBox="0 0 30 30"
    >
      <defs />
      <circle cx="15" cy="15" r="15" style={{ fill: '#ccc' }} />
      <text
        id="_"
        data-name="+"
        transform="translate(6 22.949) scale(0.507 0.505)"
        style={{
          fontSize: '59.463px',
          fill: '#fff',
          fontFamily: 'HelveticaNeueLTStd-Lt, HelveticaNeueLTStd'
        }}
      >
        +
      </text>
    </svg>
  </div>
);

export default PlusButton;

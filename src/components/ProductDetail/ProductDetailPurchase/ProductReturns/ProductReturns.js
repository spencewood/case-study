import React from 'react';

import './ProductReturns.css';

const ProductReturns = () => (
  <div className="row product-returns">
    <div className="col-3 col-lg-2 product-returns--returns-title">returns</div>
    <div className="col product-returns--returns-copy">
      This item must be returned within 30 days of the ship date. See return
      policy for details. Prices, promotions, styles and availability may vary
      by store and online.
    </div>
  </div>
);

export default ProductReturns;

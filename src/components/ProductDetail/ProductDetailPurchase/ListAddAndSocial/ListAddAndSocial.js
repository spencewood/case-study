import React from 'react';

import './ListAddAndSocial.css';

const ListAddAndSocial = () => (
  <div className="list-add-and-social">
    <div className="row">
      <div className="col small-gutter">
        <button className="list-add-and-social--button">Add to Registry</button>
      </div>
      <div className="col small-gutter">
        <button className="list-add-and-social--button">Add to List</button>
      </div>
      <div className="col small-gutter">
        <button className="list-add-and-social--button">Share</button>
      </div>
    </div>
  </div>
);

export default ListAddAndSocial;

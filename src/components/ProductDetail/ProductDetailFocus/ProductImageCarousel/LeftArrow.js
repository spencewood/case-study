import React from 'react';

const LeftArrow = ({ onClick }) => (
  <div className="product-image-carousel--button product-image-carousel--button__left">
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="10"
      height="19"
      viewBox="0 0 10 19"
      onClick={onClick}
    >
      <image
        id="Arrow_Left"
        data-name="Arrow Left"
        width="10"
        height="19"
        href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAATCAQAAAAD4lRlAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAHdElNRQfiBAENCTQiyg0jAAAAoElEQVQY023QMQtBcRjF4eOKwXInkWRiNP0Gu1nXKJlvZLPdzWryDXwARfkCZGS4JYNCKZPBpKxi9X95x2c4nfNK5qixTBiqa6LAEFeqLgWcqVi6UHapzYGSSx2OFF0K2ZF3qcuerC37IGcHeJprTNJiqKempB2MX+rprtk3e1L8Vl8nLcjYbDFi9Y+HrPF/OWKLL9l/DtRSwzS8bQopNT9GdSZTm5fcaQAAAABJRU5ErkJggg=="
      />
    </svg>
  </div>
);

export default LeftArrow;

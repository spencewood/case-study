import React from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-slick';
import LeftArrow from './LeftArrow';
import RightArrow from './RightArrow';

import './ProductImageCarousel.css';

const ProductImageCarousel = ({
  speed,
  slidesToShow,
  slidesToScroll,
  infinite,
  children
}) => (
  <div className="product-image-carousel">
    <Slider
      speed={speed}
      slidesToShow={slidesToShow}
      slidesToScroll={slidesToScroll}
      infinite={infinite}
      nextArrow={<RightArrow />}
      prevArrow={<LeftArrow />}
    >
      {children}
    </Slider>
  </div>
);

ProductImageCarousel.propTypes = {
  speed: PropTypes.number,
  slidesToShow: PropTypes.number,
  slidesToScroll: PropTypes.number,
  dots: PropTypes.bool,
  infinite: PropTypes.bool
};

ProductImageCarousel.defaultProps = {
  speed: 500,
  slidesToShow: 3,
  slidesToScroll: 3,
  infinite: true
};

export default ProductImageCarousel;

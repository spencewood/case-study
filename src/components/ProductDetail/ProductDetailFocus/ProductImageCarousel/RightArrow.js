import React from 'react';

const RightArrow = ({ onClick }) => (
  <div className="product-image-carousel--button product-image-carousel--button__right">
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="10"
      height="19"
      viewBox="0 0 10 19"
      onClick={onClick}
    >
      <image
        id="Arrow_Right"
        data-name="Arrow Right"
        width="10"
        height="19"
        href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAATCAQAAAAD4lRlAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAHdElNRQfiBAENCgu/gXPdAAAAoUlEQVQY02Mw3mtszoAGmKXOM6yVuvz8Ppq4sa7xQ2MndNUMxqrGt439MIVVjO9hE5YzvmYciSksY3zTOBpTWML4gnEqAwMjmrAow16GqYwYqsUZbqGrZGZYxPCTCUWIjWElwxeGVCYUoTUMrxkyzv6FCxpzMWxguMWQdfY/QhWX8T7jDlTj+Y0PGNejC500rkAVEjE+YVyI7the40x0DwAAB/Qo4omvWSQAAAAASUVORK5CYII="
      />
    </svg>
  </div>
);

export default RightArrow;

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ProductImageCarousel from './ProductImageCarousel';
import Magnify from './Magnify';
import './ProductDetailFocus.css';

const combinePrimaryWithAlternates = (primary, alternates) => [
  primary,
  ...alternates
];

class ProductDetailFocus extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selected: 0
    };
  }

  selectThumb = idx => {
    this.setState({
      selected: idx
    });
  };

  render() {
    const { productName, imageUrl, alternateImageUrls } = this.props;
    const images = combinePrimaryWithAlternates(imageUrl, alternateImageUrls);
    return (
      <div>
        <div className="product-detail-focus">
          <h1 className="product-detail-focus--title">{productName}</h1>
          <div className="row">
            <div className="col product-detail-focus--image">
              <img
                src={images[this.state.selected]}
                alt="focus"
                width="400"
                height="400"
              />
            </div>
          </div>
          <div className="row">
            <div className="col d-none d-lg-block d-xl-block">
              <a href="#">
                <Magnify />
                <span className="product-detail-focus--view-larger">
                  view larger
                </span>
              </a>
            </div>
          </div>
          <div className="row">
            <div className="col product-detail-focus-carousel">
              <ProductImageCarousel>
                {images.map((url, idx) => (
                  <div
                    key={idx}
                    className={`${
                      idx === this.state.selected
                        ? 'carousel-image--selected'
                        : ''
                    }`}
                    onClick={() => this.selectThumb(idx)}
                  >
                    <img src={url} className="carousel-image" alt="thumbnail" />
                  </div>
                ))}
              </ProductImageCarousel>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ProductDetailFocus.propTypes = {
  productName: PropTypes.string,
  imageUrl: PropTypes.string,
  alternateImageUrls: PropTypes.arrayOf(PropTypes.string)
};

export default ProductDetailFocus;

import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import ProductDetail from './components/ProductDetail';
import app from './reducers';

const store = createStore(app, applyMiddleware(thunk));

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className="container">
          <ProductDetail />
        </div>
      </Provider>
    );
  }
}

export default App;
